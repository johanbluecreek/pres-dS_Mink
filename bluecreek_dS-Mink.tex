\documentclass[9pt,hideallsubsections,aspectratio=43]{beamer}

\input{bluecreek_pres-def.tex}


% Title font and size packages:
%\usepackage[latin1]{inputenc}
%\usepackage{aurical}
%\usepackage{anyfontsize}

\tikzfading[name=fade left,left color=cyan!0, right color=yellow!100]

\tikzfading[name=fade right,left color=yellow!100, bottom color=cyan!0]

\newcommand{\ece}[1]{\textrm{\textcolor{emphcolor}{${#1}$}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Title
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Boundaries of the classical de Sitter landscape\\ {\small-- A constructive no-go that brings Minkowski --}}
%%% Boundaries of the classical de Sitter landscape

\author{Johan Bl{\aa}b{\"a}ck}
\institute{IPhT CEA-Saclay\\
  \vspace{5pt} {Based on:}\\
  \begin{center}
    \textcolor{arxivcolor}{arXiv:}\\
    \textcolor{arxivcolor}{1609.00385 \& 1609.00729}\\
    \vspace{5pt}
    with:\\
    \textcolor{namecolor}{David Andriot and Thomas Van Riet}\\
    %\vspace{5pt} and work in progress with:\\
    %\textcolor{namecolor}{}
    \vspace{5pt}
    {2017-02-15}\\
    @ University of Rome Tor Vergata, Rome, Italy\\
    \vspace{5pt}
    \textcolor{orange}{\texttt{https://github.com/johanbluecreek/pres-dS\_Mink/}}
  \end{center}
}

\date{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Begin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Title-frame
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[plain]
 \titlepage

% \begin{tikzpicture}[remember picture,overlay]
%  \node [xshift=-1.4cm,yshift=.55cm] at (current page.south east)
%    {\includegraphics[scale=0.05]{JTF.jpg}};
%\end{tikzpicture}
% \begin{tikzpicture}[remember picture,overlay]
%  \node [xshift=-4.8cm,yshift=.60cm] at (current page.south east)
%    {\includegraphics[scale=0.3]{logo_eurotalents.png}};
%\end{tikzpicture}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% TOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{frame}[plain]\frametitle{Outline}
%   \tableofcontents[hideallsubsections]
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% BULK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
%% Intro.
%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

\begin{frame}[plain]
\frametitle{\insertsection\!\!$\ $  -- Motivation}

\begin{block}{}
  \ec{Why} the interest in \ec{de Sitter} spacetimes?
\end{block}

\begin{block}{}
  We live in a universe that has an \ec{accelerated expansion} that is best \ec{modelled by a positive cosmological constant}, that is de Sitter.
\end{block}

\begin{block}{}
  Why \ec{string theory?}
\end{block}

\begin{block}{}
  If string theory is the/allows a \ec{fundamental description} of our universe, \ec{de Sitter should emerge} from that framework. Realising a de Sitter in string theory could potentially tell us something fundamentally new about the cosmological constant.
\end{block}


\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection\!\!$\ $  -- Motivation}

\begin{block}{}
  This is \ec{not a new idea}/approach, or question, it has been around for a while.
\end{block}

\begin{block}{}
  So \ec{why isn't it solved yet?}
\end{block}

\begin{block}{}
  At some point there was the problem of \ec{moduli stabilisation}: \ec{Compactifications} generically lead to massless scalars, that should be stabilised, that is given a mass, in order to avoid problems with e.g. observations or de-compactification. Strongly coupled strings?\footnote{\ac{Phys.Lett. 162B (1985)}: \nc{M. Dine, N. Seiberg}}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection\!\!$\ $  -- Motivation}

\begin{block}{}
  In comes \ec{fluxes} to save the day.
\end{block}

\begin{block}{}
  Fluxes allows for \ec{stabilisation of moduli in a regime of string theory} where we can do computations: \ec{supergravity}.
\end{block}

\begin{block}{}
  However, finding a de Sitter was still a problem: \ec{Fluxes were not enough} to ``lift'' backgrounds to de Sitter, negative energy contributions are necessary, e.g. orientifold-planes.\footnote{\ac{arXiv:hep-th/0007018}: \nc{J. Maldacena, C. Nu\~nez}}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection\!\!$\ $  -- Motivation}

\begin{block}{}
  But even with fluxes and orientifold-planes it is \ec{still very hard}.
\end{block}

\begin{block}{}
  The \ec{KKLT}\footnote{\ac{arXiv:hep-th/0301240}: \nc{S. Kachru, R. Kallosh, A. Linde, S. Trivedi}} scenario that has been met with some success, however has since the discovery of a singularity in the fluxes\footnote{\ac{arXiv:0912.3519}: \nc{I. Bena, M. Gra\~na, N. Halmagyi}} been pushed \ec{further away from supergravity}, and other stringy features becomes necessary.\footnote{\ac{arXiv:1412.5702}: \nc{B. Michel, E. Mintun, J. Polchinski, A. Puhm, P. Saad}; \ac{arXiv:1602.05959}: \nc{I. Bena, J.B., D. Turton}}
\end{block}

\begin{block}{}
  \ec{Other approaches} are using for example non-geometric\footnote{\ac{arXiv:1212.4984}: \nc{U. Danielsson, G. Dibitetto}; \ac{arXiv:1301.7073}: \nc{J.B., U. Danielsson, G. Dibitetto}}, or non-perturbative\footnote{\ac{ arXiv:1312.5328}: \nc{J.B., D. Roest, I. Zavala}; \ac{arXiv:1406.4866}: \nc{R. Kallosh, A. Linde, B. Vercnocke, T. Wrase}} contributions in 4 dimensional effective theories where we do not have a ten-dimensional understanding of the building blocks.
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection\!\!$\ $  -- Motivation}

\begin{block}{}
  Why is it so hard? Where should we be looking?
\end{block}

\begin{block}{}
  What about classical/simple de Sitter solutions?
\end{block}

\begin{block}{}
  One popular direction is to consider compactifications with \ec{intersecting sources}. While there are de Sitters here, none of them a (meta-)stable.\footnote{\nc{C. Caviezel, P. Koerber, S. Kors, D. L\"ust, T. Wrase and M. Zagermann, R. Flauger, S. Paban, D. Robbins, U. H. Danielsson, S. S. Haque, G. Shiu, T. Van Riet, P. Koerber, \ldots}}
\end{block}

\begin{block}{}
  One approach is to trying to figure out \ec{why these always fail to be stable}, although it is still not clear.\footnote{\nc{L. Covi, M. Gomez-Reino, C. Gross, J. Louis, G. A. Palma, C. A. Scrucca, G. Shiu, Y. Sumitomo, U. H. Danielsson, T. Van Riet, T. Wrase, D. Junghans, \ldots}}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection\!\!$\ $  -- Motivation}

\begin{block}{}
  With \ec{parallel sources} then? Can we completely rule out de Sitter?
\end{block}

\begin{block}{}
  \ec{GKP}\footnote{\ac{arXiv:hep-th/0105097}: \nc{S. Giddings, S. Kachru, J. Polshinski}} incidentally stumbled upon a sort of de Sitter no-go when investigating Minkowski solutions
  \begin{equation}
    \begin{split}
      2 e^{-2A} \tilde{{\cal R}}_4 &= - \left|*_{\bot}H - e^{\phi} F_{3} \right|^2 - 2 e^{2\phi}\left| g_s^{-1} \tilde{*}_{\bot} \d (e^{-4A} - \alpha) \right|^2 \\
      &\quad - 2 e^{-2A} \left(\d \left(e^{8A} \tilde{*} \d (e^{-4A} -  \alpha \right)\right)_{\widetilde{\bot}}
    \end{split}
  \end{equation}
  from this we can directly see that since \ec{the squares give negative semi-definite} contributions to $\tilde{{\cal R}}_4$, only Minkowski and AdS solutions are available.
\end{block}

\begin{block}{}
  These type of equations formulated as BPS-squares are also more general.\footnote{\ac{arXiv:1009.1877}: \nc{J.B., U. Danielsson, D. Junghans, T. Van Riet, T. Wrase, M. Zagermann}}
\end{block}

\end{frame}

\section{Setup}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  So this is our goal: Find the most general
  \begin{equation}
    R_4 \sim - \sum (\textrm{BPS})^2 - (\textrm{Total derivative}) + (\textrm{Other\ldots?})
  \end{equation}
  and see what definite conclusions we can draw from it.
\end{block}

\begin{block}{}
  The paper is very technical -- will avoid as much as possible to flash long equations in the derivation we will come to later.
\end{block}

\begin{block}{}
  So lets go through the details of what is needed\ldots
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{Supergravity}
  We will be considering \ec{type II Supergravity} in string-frame; IIA and IIB where appropriate, compactified down to four dimensions.
\end{block}

\begin{block}{Sources}
  We will consider D$p$-brane and O$p$-plane sources that are: \ec{Parallel}, of a \ec{single dimensionality} at a time, and are \ec{spacetime-filling} to preserve Maximal symmetry in 4 dimensions\\
  \vspace{9pt}
  We \ec{exclude NS$5$-branes and KK-monopoles}.\\
  \vspace{9pt}
  The sources used are BPS, \ece{T_p = \mu_p}, and the worldvolume dynamics is fixed to have \ece{\mathcal{F}_2 - \imath^\star[b] = 0}.
\end{block}

\begin{block}{Fluxes}
  The \ec{RR and NSNS bulk fluxes are kept general}. Due to our compactification goal that means $F_{1,2,3}$ and $H_3$ has to be internal.
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{Geometry}
  The ten-dimensional metric is split as a \ec{warped direct product} between external and internal components
  \begin{equation}
    \d s_{10}^2 = e^{2A(y)} \d s^2_4 + g_{mn} \d y^m \d y^n
  \end{equation}
  \ec{We will work locally} on our manifold, such that we can write
  \begin{equation}
    \d s_6^2 = \delta_{ab} e^a e^b
  \end{equation}
  such that they can be split in \ec{parallel and transverse indices} $a = \{a_{||}, a_{\bot}\}$, and the one-forms are $e^{a_{\bot,||}} = e^{a_{\bot,||}}_m \d y^m$.\\
  \vspace{9pt}
  There are \ec{no assumptions made on the coordinate dependence} of $e^a_m$.\\
  \vspace{9pt}
  The \ec{warping} is accounted for by the following Ansatz
  \begin{equation}
    \d s^2_{10} = e^{2A} (\d s_4^2 + \d s^2_{6||}) + e^{-2A} \d s^2_{6\bot}
  \end{equation}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{\ldots~more geometry}

  \ec{Globally} we assume the presence of \ec{volume-forms}, e.g.
  \begin{equation}
    \textrm{vol}_{||} = \epsilon_{a_{1||},\ldots a_{(p-3)||}} e^{a_{1||}} \w \ldots e^{a_{(p-3)||}}
  \end{equation}
  such that $\textrm{vol}_4 \w \textrm{vol}_{||} \w \textrm{vol}_\bot = \textrm{vol}_{10}$.\\
  \vspace{9pt}
  That is: \ec{Globally} there is a \ec{split between parallel and transverse} directions, but we do not assume the one-form basis to be global.\\
  \vspace{9pt}
  The space is also taken to be one \ec{without boundary}, such that we can apply Stokes theorem
  \begin{equation}
    \int_{\mathcal{M}} \d \star_6 \d \alpha = \int_{\partial \mathcal{M} = 0} \star_6 \d \alpha = 0
  \end{equation}
\end{block}

\end{frame}

\section{The no-go}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  Deriving the no-go requires a large amount of manipulations. This is the \ec{spirit of the derivation}.
\end{block}

\begin{block}{}
  The first observations we can make regarding the sought expression is:
  \begin{itemize}
    \item No source-term
    \item A square term of the form $|\star_\bot H_3 - F_{6-p}|^2$
  \end{itemize}
  while the \ec{non-flux equations of motion involves flux-squares}, the \ec{mixed term} must come from the \ec{Bianchi identity}
  \begin{equation}
    \d F_{8-p} = H_3 \w F_{6-p} - \mu_p \delta
  \end{equation}
  so the Bianchi identity can be used to \ec{eliminate source-terms} and at the same time help to \ec{complete the squares}.
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  To \ec{definitely complete the square} of the fluxes, we use the \ec{traced external Einstein} equation -- which in string-frame has no $|H_3|^2$ term -- and combine with the dilaton equation of motion to match the numerical factors of the $H_3^2$ and $F_{(6-p)}^2$ terms.
\end{block}

\begin{block}{}
  The use of the \ec{Bianchi identity introduced $\d F_{8-p}$} into our expression.
\end{block}

\begin{block}{}
  Placing no restriction on our local one-forms $e^a$ means we have \ec{``metric-flux''} or rather \ec{``anholonomicity symbols'' $f^a_{\phantom{a}bc}$}
  \begin{equation}
    \d e^a = - \frac{1}{2} f^a_{\phantom{a}bc} e^b \w e^c
  \end{equation}
  and in the flat frame our \ec{flux takes the following form}
  \begin{equation}
    F_{k} = \frac{1}{k!} F^{(0)}_{k\ a_1\ldots a_k} e^{a_{1\bot}} \w \ldots e^{a_{k\bot}} + \frac{1}{(k-1)!} F^{(1)}_{k\ a_1\ldots a_k} e^{a_{1||}} \w e^{a_{2\bot}} \w \ldots e^{a_{k\bot}} + \ldots
  \end{equation}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  This means that we \ec{again introduce some mixed terms}
  \begin{equation}
    \imath_{a_{||}} F_k^{(1)} \w \d e^{a_{||}}
  \end{equation}
  that we can complete to a square.
\end{block}

\begin{block}{}
  The missing terms to complete this square is present in the \ec{traced parallel Einstein equations}.
\end{block}

\begin{block}{}
  Remove the source term from the traced parallel Einstein equations using the traced external Einstein equations.
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  The rest follows by itself: There are derivative terms left over, e.g. $|\d A|^2$ and $\d \star_\bot \d A$. These terms can be rewritten into a perfect square and a total derivative.
\end{block}

\begin{block}{}
  The resulting expression is
  \begin{equation}
    \begin{split}
      2 e^{-2A} \tilde{{\cal R}}_4  &= - \left|*_{\bot}H|_{\bot} + \varepsilon_p e^{\phi} F_{k-2}|_{\bot} \right|^2  - 2 e^{2\phi}\left| g_s^{-1} \tilde{*}_{\bot} \d e^{-4A} - \varepsilon_p  F_{k}^{(0)} \right|^2\\
      &\quad - \sum_{a_{||}} \left| *_{\bot}( \d e^{a_{||}})|_{\bot} - \varepsilon_p e^{\phi} (\iota_{{a_{||}}} F_k^{(1)} ) \right|^2\ - 2 {\cal R}_{||} - 2 {\cal R}_{||}^{\bot}\\
      &\quad - 2 e^{-2A} g_s \left(\d \left(e^{8A} \left(g_s^{-1}\tilde{*}_{\bot} \d e^{-4A} - \varepsilon_p F_{k}^{(0)}\right) \right)\right)_{\widetilde{\bot}}\\
      &\quad - e^{2\phi} \Big( |F_{k}|^2 - |F_{k}^{(0)}|^2 - |F_{k}^{(1)}|^2 + 2 |F_{k+2}|^2\\&\qquad\qquad + (p-5) |F_{k+4}|^2  + \frac{1}{2} (|F_{5}|_{\bot}|^2 - |(*_6 F_{5})|_{\bot}|^2) \Big)
    \end{split}
  \end{equation}

\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  Two things of (apparent) indefinite sign: \ec{Flux-terms}, and \ec{curvature terms}.
\end{block}

\begin{block}{Fluxes}
  For each $p$ the flux-terms take the form
  \begin{align*}
    & p=3:\ - ( \mbox{fluxes} ) = 0 \\
    & p=4:\ - ( \mbox{fluxes} ) = - 2 |F_{6}|^2 \\
    & p=5:\ - ( \mbox{fluxes} ) = - \Big( |F_{3}|^2 - |F_{3}^{(0)}|^2 - |F_{3}^{(1)}|^2 + 2 |F_{5}|^2 - \frac{1}{2} |(*_6 F_{5})|_{\bot}|^2 \Big) \\
    & p=6:\ - ( \mbox{fluxes} ) = - \Big( |F_{2}|^2 - |F_{2}^{(0)}|^2 - |F_{2}^{(1)}|^2 + 2 |F_{4}|^2 + |F_{6}|^2 \Big) \\
    & p=7:\ - ( \mbox{fluxes} ) = - 2 \Big( |F_{3}|^2 + |F_{5}|^2 - \frac{1}{4} |(*_6 F_{5})|_{\bot}|^2 \Big)\\
    & p=8:\ - ( \mbox{fluxes} ) = - \Big( 2 |F_{2}|^2 + 3 |F_{4}|^2 \Big) \ .
  \end{align*}
\end{block}

\begin{block}{}
  Fluxes end up having definite signs for any $p$: \ec{all negative.}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{Curvature terms}
  The explicit expression for the curvature terms are
\begin{equation*}
  \begin{split}
    2 {\cal R}_{||} &= 2 \delta^{cd} \partial_{c_{||}} f^{a_{||}}{}_{d_{||}a_{||}} -\delta^{ab} f^{d_{||}}{}_{c_{||}a_{||}} f^{c_{||}}{}_{d_{||}b_{||}}- \frac{1}{2} \delta^{ch}\delta^{dj}\delta_{ab} f^{a_{||}}{}_{c_{||}j_{||}} f^{b_{||}}{}_{h_{||}d_{||}}\\
    2 {\cal R}_{||}^{\bot}  &=  -\delta^{ab} f^{d_{\bot}}{}_{c_{\bot}a_{||}} f^{c_{\bot}}{}_{d_{\bot}b_{||}} - \delta^{ab} \delta^{dg} \delta_{ch} f^{h_{\bot}}{}_{g_{\bot}a_{||}} f^{c_{\bot}}{}_{d_{\bot}b_{||}} \\
    &\quad - 2 \delta^{ab} f^{d_{\bot}}{}_{c_{||}a_{||}} f^{c_{||}}{}_{d_{\bot}b_{||}} - \delta^{ab} \delta^{dg} \delta_{ch} f^{h_{\bot}}{}_{g_{||}a_{||}} f^{c_{\bot}}{}_{d_{||}b_{||}}
  \end{split}
\end{equation*}
\end{block}

\begin{block}{}
\begin{itemize}
  \item[${\cal R}_{||}$:] Curvature of the wrapped internal subspace. Wrapping tori, or $p=3,4$ this is trivially zero.
  \item[${\cal R}_{||}^{\bot}$:] $f^{a_{\bot}}{}_{b_{||}c_{||}}$ encodes the fibration of the transverse subspace over the parallel base subspace. Wrapping fibers, $p=4$, group manifolds and orientifold projections sets parts of this to zero.
\end{itemize}
\end{block}

\begin{block}{}
These are generally not considered, but we cannot rule them out.
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  We can however put a constraint on them.
\end{block}

\begin{block}{}
  Only if these inequalities are satisfied there is a possibility of de Sitter
  \begin{equation}
    -\frac{1}{2} \int_{\widetilde{\cal M}} \widetilde{{\rm vol}}_{6}\, e^{2A}\,  \sum_{a_{||}} |(\d e^{a_{||}})|_{\bot}|^2\, < \, \int_{\widetilde{\cal M}} \widetilde{{\rm vol}}_{6}\, e^{2A} \left( {\cal R}_{||} + {\cal R}_{||}^{\bot} \right)\, < 0
  \end{equation}
\end{block}

\begin{block}{}
  This concludes the de Sitter no-go: We find that classical de Sitter is only possible within a limited range for a particular set of curvature terms, for $p=4,5,6$.
\end{block}

\begin{block}{}
  What about $p=3,7,8$?
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  For $p=3$ the curvature terms are trivially zero because of the \ec{lack of internal parallel directions}.\\
  \vspace{9pt}
  With $F_1^2$ not contributing, we can rule out $p=3$ completely.
\end{block}

\begin{block}{}
  We are able to derive
  \begin{equation}
    \begin{split}
      \frac{(p-3)}{e^{2A}} \tilde{{\cal R}}_4  = & - 2 |H|^2 + e^{2\phi} \left( (8-p) |F_0|^2 + (6-p) |F_2|^2 + (4-p) |F_4|^2 \right.\\&\left.+ (2-p) |F_6|^2 \right)\\
      \frac{(p-3)}{e^{2A}} \tilde{{\cal R}}_4  = & - 2 |H|^2 + e^{2\phi} \left( (7-p) |F_1|^2 + (5-p) |F_3|^2 + (3-p) |F_5|^2 \right)
    \end{split}
  \end{equation}
  which completely excludes de Sitter for $p=7,8$.\footnote{\ac{arXiv:0907.2041}: \nc{U. H. Danielsson, S. S. Haque, G. Shiu and T. Van Riet}; \ac{arXiv:1003.0029}: \nc{T. Wrase and M. Zagermann}}
\end{block}


\end{frame}

\section{Minkowski}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  Our derived expression also opens up a possibility of a broad class of Minkowski solutions.
\end{block}

\begin{block}{}
  By demanding
  \begin{equation}
    \begin{split}
      F_{6-p}       &= - e^{-\phi} \varepsilon_p *_{\bot}H\\
      F_{8-p}^{(0)} &= e^{-\phi} \varepsilon_p e^{4A} *_{\bot} \d e^{-4A}\\
      F_{8-p}^{(1)} &= e^{-\phi} \varepsilon_p\, \delta_{ab}\, e^{a_{||}} \w *_{\bot}( \d e^{b_{||}})|_{\bot}
    \end{split}
  \end{equation}
  which together with $(\textrm{fluxes})^2 = {\cal R}_{||} = {\cal R}_{||}^{\bot} = 0$ is compatible with Minkowski.
\end{block}

\begin{block}{}
  The first two equations are equivalent to \ec{GKP} for $p=3$, while the last two are equivalent to Minkowski from compactifications with \ec{twisted tori}.\footnote{\ac{arXiv:hep-th/0211182}: \nc{S. Kachru, M. Schulz, P. Tripathy, S. Trivedi}}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  These conditions are however \ec{not enough}.
\end{block}

\begin{block}{}
  Generally in order to find an \ec{explicit solution} to the equations of motion, one would have to specify an \ec{explicit metric}.
\end{block}

\begin{block}{}
  However, as is common, we will be satisfied with a \ec{solution up to one differential equation}.
\end{block}

\begin{block}{}
  That is: \ec{We solve all equations} of motion using
  \begin{align}
  e^{\phi}\frac{T_{10}}{p+1} & = e^{6A} \tilde{\Delta}_{\bot} e^{-4A}  + e^{2\phi}|F_{6-p}|^2 + e^{2\phi}|F_{8-p}^{(1)}|^2 \label{BIrew}\\
  & = e^{6A} \tilde{\Delta}_{\bot} e^{-4A}  + |H|^2 + |f^{a_{||}}{}_{b_{\bot}c_{\bot}}|^2 \ ,\label{BINS}
\end{align}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  Our no-go equation also offers natural extension.
\end{block}

\begin{block}{}
  Non-geometric fluxes are generated through T-dualities as
  \begin{equation}
    H_{a_\bot b_\bot c_\bot} \to f^{a_{||}}_{\phantom{a_{||}}b_\bot c_\bot} \to Q^{a_{||}b_{||}}_{\phantom{a_{||}b_{||}}c_\bot} \to R^{a_{||}b_{||}c_{||}}
  \end{equation}
\end{block}

\begin{block}{}
  Word of caution: Working with non-geometric fluxes from our ten-dimensional point of view is of course problematic.\\
  \vspace{9pt}
  \ec{This is purely conjectural}\\
  \vspace{9pt}
  To properly \ec{derive} these expressions, one would have more control in 4d gauged sugra or in $\beta$-supergravity\footnote{\ac{arXiv:1306.4381}: \nc{D. Andriot, A. Betz}}.
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  We can then use this to generalise our expression
  \begin{equation}
    \begin{split}
      p \geq 3:\qquad - &\left| H_{a_{\bot}b_{\bot}c_{\bot}} *_{\bot}e^{a_{\bot} b_{\bot} c_{\bot}}/3 + \varepsilon_p e^{\phi} F_{6-p}^{(0)} \right|^2\\
      &\qquad \downarrow \qquad\qquad\qquad\qquad\qquad \downarrow\\
      p \geq 4:\ - \sum_{a_{||}} &\left|f^{a_{||}}{}_{b_{\bot}c_{\bot}} *_{\bot} e^{ b_{\bot} c_{\bot}}/2 + \varepsilon_p e^{\phi} (\iota_{{a_{||}}} F_{8-p}^{(1)} ) \right|^2\\
      &\qquad \downarrow \qquad\qquad\qquad\qquad\qquad \downarrow\\
      p \geq 5:\ -\! \sum_{a_{||}b_{||}}\! &\left|Q_{c_{\bot}}{}^{a_{||}b_{||}} *_{\bot} e^{c_{\bot}} + \varepsilon_p e^{\phi} (\iota_{{b_{||}}} \iota_{{a_{||}}} F_{10-p}^{(2)} ) \right|^2\\
      &\qquad \downarrow \qquad\qquad\qquad\qquad\qquad \downarrow\\
      p \geq 6:\ - \!\!\! \sum_{a_{||} b_{||} c_{||}}\!\!\! &\left|R^{a_{||} b_{||}c_{||}} *_{\bot} 1 + \varepsilon_p e^{\phi} (\iota_{{c_{||}}} \iota_{{b_{||}}} \iota_{{a_{||}}} F_{12-p}^{(3)} ) \right|^2
    \end{split}
  \end{equation}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection}

\begin{block}{}
  The ``no-go equation'' generalised
  \begin{align}
  2 e^{-2A} \tilde{{\cal R}}_4 & = - 2 \left| e^{4A} *_{\bot} \d e^{-4A} - \varepsilon_p e^{\phi}  F_{8-p}^{(0)} \right|^2 - (\mbox{flux})^2  + e^{-2A} \partial(\dots)\\
  & - \left| H_{a_{\bot}b_{\bot}c_{\bot}} *_{\bot}e^{a_{\bot} b_{\bot} c_{\bot}}/3 + \varepsilon_p e^{\phi} F_{6-p}^{(0)} \right|^2 \\
  & - \sum_{a_{||}} \left|f^{a_{||}}{}_{b_{\bot}c_{\bot}} *_{\bot} e^{ b_{\bot} c_{\bot}}/2 + \varepsilon_p e^{\phi} (\iota_{{a_{||}}} F_{8-p}^{(1)} ) \right|^2  \\
  & -\! \sum_{a_{||}b_{||}}\! \left|Q_{c_{\bot}}{}^{a_{||}b_{||}} *_{\bot} e^{c_{\bot}} + \varepsilon_p e^{\phi} (\iota_{{b_{||}}} \iota_{{a_{||}}} F_{10-p}^{(2)} ) \right|^2  \\
  & - \!\!\! \sum_{a_{||} b_{||} c_{||}}\!\!\! \left|R^{a_{||} b_{||}c_{||}} *_{\bot} 1 + \varepsilon_p e^{\phi} (\iota_{{c_{||}}} \iota_{{b_{||}}} \iota_{{a_{||}}} F_{12-p}^{(3)} ) \right|^2
  \end{align}
which should bring Minkowski
\begin{equation}
  e^{\phi}\frac{T_{10}}{p+1} = e^{6A} \tilde{\Delta}_{\bot} e^{-4A}  + |H|^2 + |f^{a_{||}}{}_{b_{\bot}c_{\bot}}|^2 + |Q_{c_{\bot}}{}^{a_{||}b_{||}}|^2 + |R^{a_{||} b_{||}c_{||}}|^2
\end{equation}
\end{block}

\end{frame}

\section{Epilogue}

\begin{frame}[plain]
\frametitle{\insertsection \ -- Summary}

\begin{block}{de Sitter}
  Deriving an expression of negative (semi)-definite squares and other terms we are able to conclude
  \begin{itemize}
    \item $p=3$ is ruled out -- $F_1$ does not alter the GKP story.
    \item $p=4,5,6$ are ruled out except for a small range for a particular set of curvature terms.
    \item $p=7,8$ are ruled out completely.
  \end{itemize}
\end{block}

\begin{block}{Minkowski}
  The \ec{semi}-definiteness of the squares lead us to look for Minkowski solutions.
  \begin{itemize}
    \item Together with the $F_{8-p}$ B.I. all equations of motion are solved.
    \item E.o.m.s are solved in a manner of ``superposition''.
    \item We generalise and conjecture a set of non-geometric Minkowski solutions for $p\geq 5$.
  \end{itemize}
\end{block}

\end{frame}

\begin{frame}[plain]
\frametitle{\insertsection \ -- Outlook}

\begin{block}{de Sitter}
  \begin{itemize}
    \item Is it possible to rule out de Sitter for parallel sources completely?
    \item Can stability rule out de Sitter completely here?
  \end{itemize}
\end{block}

\begin{block}{Minkowski}
  \begin{itemize}
    \item Can we verify our non-geometric expression from a 4d point of view? Is it valid?
  \end{itemize}
\end{block}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% End frame
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section*{}

\begin{frame}[plain]
\frametitle{\phantom{a}}

\begin{center}
{\Large Thank you for your attention.}\\
\vspace{2cm}
Presentation available at
\textcolor{orange}{\texttt{https://github.com/johanbluecreek/pres-dS\_Mink/}}
\end{center}

\end{frame}

















\end{document}
