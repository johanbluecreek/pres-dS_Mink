# dS no-go and Minkowski solutions

## Abstract

Embedding de Sitter solutions in string theory is hard. If they are at all possible to construct, they often happen to be unstable. Restricting to a particular set of compactification using parallel sources, we are able to derive an expression that excludes the presence of de Sitter, without a stability analysis, except for a bounded range for a set of curvature terms. This expression also leads to a broad class of Minkowski solutions that I will describe. In this talk, I will motivate the derivation of this expression, and describe how to derive it and in detail go through the underlying assumptions and conclusions that can be drawn from it.

## Information

This presentation is based on the papers:

* [arXiv:1609.00385](https://arxiv.org/abs/1609.00385)
* [arXiv:1609.00729](https://arxiv.org/abs/1609.00729)

This presentation was/will be given at:

* String Theory Group, INFN & Physics Department, University of Rome Tor Vergata, Rome, Italy @ 2017-02-15
* THEP group, Institut für Physik, Johannes-Gutenberg-Universität, Mainz, Germany @ 2017-05-02
* String Theory & Cosmology (GRS), Gordon-Kenan Research Seminar, Cosmic Origin and Cosmic Fate - From Big Bang to Dark Energy, Renaissance Tuscany Il Ciocco, Lucca (Barga), Italy @ 2017-05-28
